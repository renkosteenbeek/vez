<?php
require('IcecastConnector.php');
$ice = new IcecastConnector();
?>
<!doctype html>
<html>
<head>
    <meta name="google-site-verification" content="-8F_rpxx88T6iUQ8EVmJKxnPRfSz4OkVBb0umzohLCo" />
    <meta charset="utf-8" />
    <meta name="viewport" content="user-scalable=yes, width=device-width, minimum-scale=1, initial-scale=1, maximum-scale=1">
    <meta name="description" content="De VEZ: Evangelische kerk in Zwolle "/>


    <title>Media - VEZ</title>
    <base href="https://www.vezwolle.nl/" />


    <link rel="stylesheet" href="https://vez.gentle-innovations.nl/css/fonts.css">
    <link rel="stylesheet" href="/css/foundation.min.css">

    <link rel="stylesheet" href="/css/rs-default.css">
    <link rel="stylesheet" href="/css/app.css?v=1.1.0.14"">

    <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon" />

    <script src="/js/modernizr.foundation.js"></script>

    <!-- Included JS Files (Compressed) -->
    <script src="/js/foundation.min.js"></script>

    <script type="text/javascript" src="/golive/minify/?f=/golive/js/cmsScripts.js"></script>

    <? if (!$ice->isStreaming() AND date('N') == 7 ): ?>
        <meta http-equiv="refresh" content="30">
    <?endif?>

    <script>

        (function( $ ){
            $.fn.mediaelementplayer = function() {
                return this;
            };
        })( jQuery );

        $(document).ready(function() {
            var pathname = window.location.href.split('#')[0];
            $('a[href^="#"]').each(function() {

                var $this = $(this),
                    link = $this.attr('href');
                if(!$(link).is('div'))
                    $this.attr('href', pathname + link);
            });
        });
    </script>
</head>

<body>

<!-- Mobile Navigation -->
<nav class="top-bar nav-mobile show-for-small">
    <ul>
        <!-- Title Area -->
        <li class="name">
            <div>
                <a href="#">
                    Navigatie
                </a>
            </div>
        </li>
        <li class="toggle-topbar"><a href="#"></a></li>
    </ul>

    <section>
        <ul class="nav-menu left">
            <li class="divider show-for-small"></li>
            <li class=" ">
                <a href="home">Home</a>

            </li><li class="divider show-for-small"></li>
            <li class=" ">
                <a href="actueel">Actueel</a>

            </li><li class="divider show-for-small"></li>
            <li class=" has-dropdown">
                <a href="kerk-in-zwolle">Wie we zijn</a>
                <ul class="dropdown">
                    <li class="first">
                        <a>Missie & Visie</a>
                        <ul>
                            <li><a href="kerk-in-zwolle/missie-visie/missie-visie-waarden">Onze missie, visie & waarden </a></li><li><a href="kerk-in-zwolle/missie-visie/onze-samenwerkingen">Samenwerking</a></li><li><a href="kerk-in-zwolle/missie-visie/Onze-identiteit">Onze identiteit</a></li>
                        </ul>
                    </li><li class="second">
                        <a>Geschiedenis</a>
                        <ul>
                            <li><a href="kerk-in-zwolle/geschiedenis/Waar-komen-we-vandaan">Waar komen we vandaan?</a></li><li><a href="kerk-in-zwolle/geschiedenis/De-VEZ-vandaag ">De VEZ vandaag</a></li>
                        </ul>
                    </li><li class="third">
                        <a>Organisatie</a>
                        <ul>
                            <li><a href="kerk-in-zwolle/organisatie/leidersteam">VEZ Organisatie</a></li><li><a href="kerk-in-zwolle/organisatie/">Vacature Pastor algemene zaken</a></li><li><a href="kerk-in-zwolle/organisatie/organogram">Organogram</a></li><li><a href="kerk-in-zwolle/organisatie/financien">Financien</a></li><li><a href="kerk-in-zwolle/organisatie/vez-en-anbi">VEZ ANBI & VEZ beleidsplan</a></li><li><a href="kerk-in-zwolle/organisatie/veilige-kerk-en-vertrouwenspersonen">Veilige Kerk & Vertrouwenspersonen</a></li>
                        </ul>
                    </li><li class="fourth">
                        <a>Locatie</a>
                        <ul>
                            <li><a href="kerk-in-zwolle/locatie-kerk-in-zwolle/locaties-van-de-vez">Adressen</a></li>
                        </ul>
                    </li>
                </ul>
            </li><li class="divider show-for-small"></li>
            <li class=" has-dropdown">
                <a href="wat-we-doen">Wat we doen</a>
                <ul class="dropdown">
                    <li class="first">
                        <a>Onderwijs | VEZ Leerhuis</a>
                        <ul>
                            <li><a href="wat-we-doen/onderwijs/vez-leerhuis">Algemene informatie</a></li><li><a href="wat-we-doen/onderwijs/cursussen">Cursussen</a></li><li><a href="wat-we-doen/onderwijs/colleges">Colleges</a></li><li><a href="wat-we-doen/onderwijs/excursies">Excursies</a></li><li><a href="wat-we-doen/onderwijs/marriage-course ">(Pre) Marriage Course in de VEZ</a></li>
                        </ul>
                    </li><li class="second">
                        <a>Samenkomsten</a>
                        <ul>
                            <li><a href="wat-we-doen/samenkomsten-kerk-in-zwolle/startzondag-2-september">Startzondag 2 september</a></li><li><a href="wat-we-doen/samenkomsten-kerk-in-zwolle/onze-diensten-kerk-in-zwolle">Diensten</a></li><li><a href="wat-we-doen/samenkomsten-kerk-in-zwolle/hersteldiensten">Hersteldiensten</a></li><li><a href="wat-we-doen/samenkomsten-kerk-in-zwolle/doopdiensten">Doopdiensten</a></li><li><a href="wat-we-doen/samenkomsten-kerk-in-zwolle/opdraagdiensten">Opdraagdiensten</a></li>
                        </ul>
                    </li><li class="third">
                        <a>Kinderen</a>
                        <ul>
                            <li><a href="wat-we-doen/kinderen/">Startavond KidsVEZtival 12 september 2017</a></li><li><a href="wat-we-doen/kinderen/schatkids-3-jr---groep-2">0 - Groep 2 BS</a></li><li><a href="wat-we-doen/kinderen/kidsveztival-schatgraven-groep-3-8">Groep 3 BS - Groep 8 BS</a></li><li><a href="wat-we-doen/kinderen/speciale-activiteiten">Activiteiten & Spaarprojecten</a></li><li><a href="wat-we-doen/kinderen/huis-van-goud">Huis van Goud</a></li>
                        </ul>
                    </li><li class="fourth">
                        <a>Jongeren</a>
                        <ul>
                            <li><a href="wat-we-doen/jongeren/allstars-12-18-jr">Allstars | 12-18 jaar</a></li><li><a href="wat-we-doen/jongeren/vision-18-25-jr">Vision! | 18-25 jaar</a></li><li><a href="wat-we-doen/jongeren/deeper">Deeper</a></li><li><a href="wat-we-doen/jongeren/glow">GLOW</a></li>
                        </ul>
                    </li><li class="first">
                        <a>Missionair</a>
                        <ul>
                            <li><a href="wat-we-doen/missionair/missionair-loket">Missionair Loket</a></li><li><a href="wat-we-doen/missionair/zendelingen">Zending </a></li><li><a href="wat-we-doen/missionair/evangelisatie">Evangelisatie</a></li><li><a href="wat-we-doen/missionair/VEZ-Straatevangelisatie">VEZ Straatevangelisatie</a></li><li><a href="wat-we-doen/missionair/vamos">Projecten (Vamos)</a></li><li><a href="wat-we-doen/missionair/partners">Partners</a></li><li><a href="wat-we-doen/missionair/alpha-cursus">Alpha Cursus</a></li>
                        </ul>
                    </li><li class="second">
                        <a>Zorg & Herstel</a>
                        <ul>
                            <li><a href="wat-we-doen/zorg-herstel/gemeentezorg-in-de-praktijk">Gemeentezorg in de praktijk</a></li><li><a href="wat-we-doen/zorg-herstel/leven-in-christus">Leven in Christus</a></li><li><a href="wat-we-doen/zorg-herstel/pastoraat">Pastoraat</a></li><li><a href="wat-we-doen/zorg-herstel/diaconaat">Diaconaat </a></li><li><a href="wat-we-doen/zorg-herstel/sleutels-tot-herstel">Sleutels tot Herstel</a></li><li><a href="wat-we-doen/zorg-herstel/refresh">ReFresh</a></li><li><a href="wat-we-doen/zorg-herstel/vez-werkplein">VEZ Werkplein</a></li><li><a href="wat-we-doen/zorg-herstel/huis-van-gebed-en-aanbidding">Huis van Gebed en Aanbidding</a></li><li><a href="wat-we-doen/zorg-herstel/mandaat7">Mandaat7</a></li>
                        </ul>
                    </li><li class="third">
                        <a>Kringen</a>
                        <ul>
                            <li><a href="wat-we-doen/kringen/huiskringen">Huiskringen</a></li><li><a href="wat-we-doen/kringen/huiskringmateriaal-seizoen-20172018">Huiskringmateriaal seizoen 2017/2018</a></li>
                        </ul>
                    </li><li class="fourth">
                        <a>Groepen & Overige initiatieven</a>
                        <ul>
                            <li><a href="wat-we-doen/groepen/vez-bieb">VEZ Bieb</a></li><li><a href="wat-we-doen/groepen/puur-2">Puur 2</a></li><li><a href="wat-we-doen/groepen/doven-in-de-vez">Doven in de VEZ</a></li><li><a href="wat-we-doen/groepen/werkverzetters">Werkverzetters</a></li><li><a href="wat-we-doen/groepen/senioren-vez">Senioren VEZ</a></li><li><a href="wat-we-doen/groepen/dubbelgoed">Dubbelgoed </a></li><li><a href="wat-we-doen/groepen/mingles">MINGLES</a></li>
                        </ul>
                    </li>
                </ul>
            </li><li class="divider show-for-small"></li>
            <li class=" has-dropdown">
                <a href="betrokken-raken">Betrokken raken</a>
                <ul class="dropdown">
                    <li class="first">
                        <a>Deelnemen</a>
                        <ul>
                            <li><a href="betrokken-raken/registreren/registratieformulier">Registreren</a></li><li><a href="betrokken-raken/registreren/Thuis in de VEZ">THUIS in de VEZ</a></li><li><a href="betrokken-raken/registreren/huiskring">Huiskring</a></li><li><a href="betrokken-raken/registreren/privacyverklaring">Privacyverklaring</a></li>
                        </ul>
                    </li><li class="second">
                        <a>Bidden & Geven</a>
                        <ul>
                            <li><a href="betrokken-raken/bidden-geven/bidden">Bidden</a></li><li><a href="betrokken-raken/bidden-geven/gebed-in-de-vez">Gebed in de VEZ</a></li><li><a href="betrokken-raken/bidden-geven/geven">Geven</a></li>
                        </ul>
                    </li><li class="third">
                        <a>VEZ Vrijwilligers </a>
                        <ul>
                            <li><a href="betrokken-raken/vez-vrijwilligers/dienen-toppunt">TOPpunt (VEZ vrijwilligersbureau)</a></li><li><a href="betrokken-raken/vez-vrijwilligers/vacatures-toppunt">TOPpunt vacatures</a></li><li><a href="betrokken-raken/vez-vrijwilligers/open-sollicitatie-toppunt">TOPpunt Open solliciteren</a></li><li><a href="betrokken-raken/vez-vrijwilligers/toppunt-spreekuur-">TOPpunt Spreekuur </a></li><li><a href="betrokken-raken/vez-vrijwilligers/toppunt-handen-pool">TOPpunt Handen Pool</a></li><li><a href="betrokken-raken/vez-vrijwilligers/schoonmaken">Schoonmaken VEZ Centrum</a></li>
                        </ul>
                    </li><li class="fourth">
                        <a>MijnKerk</a>
                        <ul>
                            <li><a href="betrokken-raken/mijnkerk/de-interactieve-kerk">Mijn Kerk (Interactieve Kerk)</a></li><li><a href="betrokken-raken/mijnkerk/mijnkerk-veelgestelde-vragen">MijnKerk: Veelgestelde vragen</a></li>
                        </ul>
                    </li>
                </ul>
            </li><li class="divider show-for-small"></li>
            <li class="active ">
                <a href="media">Media</a>

            </li><li class="divider show-for-small"></li>
            <li class=" ">
                <a href="agenda-kerk-in-zwolle">Agenda</a>

            </li><li class="divider show-for-small"></li>
            <li class=" ">
                <a href="contact">Contact</a>

            </li>
            <li class="divider show-for-small"></li>
            <li class="hide-for-small">
                <div class="menuSearch"></div>
                <div class="searchBox">
                    <div class="searchForm">
                        <form method="post" action="search">
                            <input type="text" name="search_field" id="search_field" placeholder="Type uw trefwoord" />
                            <input type="submit" value="Zoek" />
                        </form>
                        <div class="clear"></div>
                    </div>
                </div>
            </li>
        </ul>

























    </section>
</nav>

<div class="row mobile-logo show-for-small"><a href="home"><img src="img/logo-mobile.png" alt="VEZ"></a></div>

<div class="top-bar top-balk hide-for-small">
    <div class="row">
        <div class="five columns hide-for-small">
            Samenkomst zat. 19:00, zo. 09:30 &amp; 11:30 [<a href="/wat-we-doen/samenkomsten/zondagsdiensten" style="text-decoration:underline">Meer info</a>]

        </div>
        <div class="three columns text mobile-two">

        </div>
        <div class="four columns mobile-two">
            <a href="http://mijnkerk.vezwolle.nl" class="vez-kort2">GA NAAR MIJN KERK</a>
            <a href="javascript:void(0)" class="vez-kort icon-list" id="vez-top">VEZ IN HET KORT</a>
        </div>
    </div>
</div>

<div class="vez-top" id="vez-top-content">
    <div class="row">
        <div class="four columns top-block">
            <div class="inner">
                <h2>Onze missie en visie</h2>
                <img alt="" height="194" src="/userfiles/Image/mg3241.jpg" width="291" />
                <blockquote>
                    <p>De VEZ is een bruisende kerk in Zwolle met meer dan 3500 leden. Benieuwd naar onze missie en visie?&nbsp;</p>
                </blockquote>
                <a class="read-more" href="kerk-in-zwolle/missie-visie/missie-visie-waarden">Lees meer</a>
            </div>
        </div>
        <div class="four columns top-block">
            <div class="inner">
                <h2>De VEZ heeft je nodig!</h2>
                <img alt="" height="194" src="/userfiles/Image/mg3454.jpg" width="291" />
                <p>Bezoek de pagina van TopPunt om te kijken wat jij voor de VEZ kunt betekenen!&nbsp;</p>
                <a class="read-more" href="/betrokken-raken/vacatures-toppunt">Vacatures</a>
            </div>
        </div>
        <div class="four columns top-block">
            <div class="inner">
                <h2>Plaats van samenkomst</h2>
                <img src="https://maps.googleapis.com/maps/api/staticmap?center=Buitengasthuisstraat 8, Zwolle&amp;zoom=15&amp;size=291x194&amp;maptype=roadmap&amp;markers=color:red|label:S|52.515817,6.076441&amp;sensor=false" />
                <div class="eight columns">
                    <p><strong>Zaterdagavondienst</strong><br />
                        19:00 - 20:20<br />
                        <strong>Zondagochtenddienst</strong><br />
                        1e dienst: 09.30 uur - 10.50 uur<br />
                        2e dienst: 11.30 uur - 12.50 uur<br />
                        <strong>WRZV-hallen</strong><br />
                        <strong>Buitengasthuisstraat 8</strong><br />
                        <strong>Zwolle</strong></p>
                </div>

                <div class="four columns">&nbsp;
                    <p><a class="read-more" href="contact">Contact</a></p>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- Header and Nav -->
<div class="header hide-for-small">
    <div class="row">
        <div class="two columns">
            <a href="home" class="logo"><img src="img/logo.png" width="150" height="220" alt="VEZ Zwolle" /></a>
        </div>
        <div class="ten columns nav-desktop">
            <ul class="nav-menu left">
                <li class="divider show-for-small"></li>
                <li class=" ">
                    <a href="home">Home</a>

                </li><li class="divider show-for-small"></li>
                <li class=" ">
                    <a href="actueel">Actueel</a>

                </li><li class="divider show-for-small"></li>
                <li class=" has-dropdown">
                    <a href="kerk-in-zwolle">Wie we zijn</a>
                    <ul class="dropdown">
                        <li class="first">
                            <a>Missie & Visie</a>
                            <ul>
                                <li><a href="kerk-in-zwolle/missie-visie/missie-visie-waarden">Onze missie, visie & waarden </a></li><li><a href="kerk-in-zwolle/missie-visie/onze-samenwerkingen">Samenwerking</a></li><li><a href="kerk-in-zwolle/missie-visie/Onze-identiteit">Onze identiteit</a></li>
                            </ul>
                        </li><li class="second">
                            <a>Geschiedenis</a>
                            <ul>
                                <li><a href="kerk-in-zwolle/geschiedenis/Waar-komen-we-vandaan">Waar komen we vandaan?</a></li><li><a href="kerk-in-zwolle/geschiedenis/De-VEZ-vandaag ">De VEZ vandaag</a></li>
                            </ul>
                        </li><li class="third">
                            <a>Organisatie</a>
                            <ul>
                                <li><a href="kerk-in-zwolle/organisatie/leidersteam">VEZ Organisatie</a></li><li><a href="kerk-in-zwolle/organisatie/">Vacature Pastor algemene zaken</a></li><li><a href="kerk-in-zwolle/organisatie/organogram">Organogram</a></li><li><a href="kerk-in-zwolle/organisatie/financien">Financien</a></li><li><a href="kerk-in-zwolle/organisatie/vez-en-anbi">VEZ ANBI & VEZ beleidsplan</a></li><li><a href="kerk-in-zwolle/organisatie/veilige-kerk-en-vertrouwenspersonen">Veilige Kerk & Vertrouwenspersonen</a></li>
                            </ul>
                        </li><li class="fourth">
                            <a>Locatie</a>
                            <ul>
                                <li><a href="kerk-in-zwolle/locatie-kerk-in-zwolle/locaties-van-de-vez">Adressen</a></li>
                            </ul>
                        </li>
                    </ul>
                </li><li class="divider show-for-small"></li>
                <li class=" has-dropdown">
                    <a href="wat-we-doen">Wat we doen</a>
                    <ul class="dropdown">
                        <li class="first">
                            <a>Onderwijs | VEZ Leerhuis</a>
                            <ul>
                                <li><a href="wat-we-doen/onderwijs/vez-leerhuis">Algemene informatie</a></li><li><a href="wat-we-doen/onderwijs/cursussen">Cursussen</a></li><li><a href="wat-we-doen/onderwijs/colleges">Colleges</a></li><li><a href="wat-we-doen/onderwijs/excursies">Excursies</a></li><li><a href="wat-we-doen/onderwijs/marriage-course ">(Pre) Marriage Course in de VEZ</a></li>
                            </ul>
                        </li><li class="second">
                            <a>Samenkomsten</a>
                            <ul>
                                <li><a href="wat-we-doen/samenkomsten-kerk-in-zwolle/startzondag-2-september">Startzondag 2 september</a></li><li><a href="wat-we-doen/samenkomsten-kerk-in-zwolle/onze-diensten-kerk-in-zwolle">Diensten</a></li><li><a href="wat-we-doen/samenkomsten-kerk-in-zwolle/hersteldiensten">Hersteldiensten</a></li><li><a href="wat-we-doen/samenkomsten-kerk-in-zwolle/doopdiensten">Doopdiensten</a></li><li><a href="wat-we-doen/samenkomsten-kerk-in-zwolle/opdraagdiensten">Opdraagdiensten</a></li>
                            </ul>
                        </li><li class="third">
                            <a>Kinderen</a>
                            <ul>
                                <li><a href="wat-we-doen/kinderen/">Startavond KidsVEZtival 12 september 2017</a></li><li><a href="wat-we-doen/kinderen/schatkids-3-jr---groep-2">0 - Groep 2 BS</a></li><li><a href="wat-we-doen/kinderen/kidsveztival-schatgraven-groep-3-8">Groep 3 BS - Groep 8 BS</a></li><li><a href="wat-we-doen/kinderen/speciale-activiteiten">Activiteiten & Spaarprojecten</a></li><li><a href="wat-we-doen/kinderen/huis-van-goud">Huis van Goud</a></li>
                            </ul>
                        </li><li class="fourth">
                            <a>Jongeren</a>
                            <ul>
                                <li><a href="wat-we-doen/jongeren/allstars-12-18-jr">Allstars | 12-18 jaar</a></li><li><a href="wat-we-doen/jongeren/vision-18-25-jr">Vision! | 18-25 jaar</a></li><li><a href="wat-we-doen/jongeren/deeper">Deeper</a></li><li><a href="wat-we-doen/jongeren/glow">GLOW</a></li>
                            </ul>
                        </li><li class="first">
                            <a>Missionair</a>
                            <ul>
                                <li><a href="wat-we-doen/missionair/missionair-loket">Missionair Loket</a></li><li><a href="wat-we-doen/missionair/zendelingen">Zending </a></li><li><a href="wat-we-doen/missionair/evangelisatie">Evangelisatie</a></li><li><a href="wat-we-doen/missionair/VEZ-Straatevangelisatie">VEZ Straatevangelisatie</a></li><li><a href="wat-we-doen/missionair/vamos">Projecten (Vamos)</a></li><li><a href="wat-we-doen/missionair/partners">Partners</a></li><li><a href="wat-we-doen/missionair/alpha-cursus">Alpha Cursus</a></li>
                            </ul>
                        </li><li class="second">
                            <a>Zorg & Herstel</a>
                            <ul>
                                <li><a href="wat-we-doen/zorg-herstel/gemeentezorg-in-de-praktijk">Gemeentezorg in de praktijk</a></li><li><a href="wat-we-doen/zorg-herstel/leven-in-christus">Leven in Christus</a></li><li><a href="wat-we-doen/zorg-herstel/pastoraat">Pastoraat</a></li><li><a href="wat-we-doen/zorg-herstel/diaconaat">Diaconaat </a></li><li><a href="wat-we-doen/zorg-herstel/sleutels-tot-herstel">Sleutels tot Herstel</a></li><li><a href="wat-we-doen/zorg-herstel/refresh">ReFresh</a></li><li><a href="wat-we-doen/zorg-herstel/vez-werkplein">VEZ Werkplein</a></li><li><a href="wat-we-doen/zorg-herstel/huis-van-gebed-en-aanbidding">Huis van Gebed en Aanbidding</a></li><li><a href="wat-we-doen/zorg-herstel/mandaat7">Mandaat7</a></li>
                            </ul>
                        </li><li class="third">
                            <a>Kringen</a>
                            <ul>
                                <li><a href="wat-we-doen/kringen/huiskringen">Huiskringen</a></li><li><a href="wat-we-doen/kringen/huiskringmateriaal-seizoen-20172018">Huiskringmateriaal seizoen 2017/2018</a></li>
                            </ul>
                        </li><li class="fourth">
                            <a>Groepen & Overige initiatieven</a>
                            <ul>
                                <li><a href="wat-we-doen/groepen/vez-bieb">VEZ Bieb</a></li><li><a href="wat-we-doen/groepen/puur-2">Puur 2</a></li><li><a href="wat-we-doen/groepen/doven-in-de-vez">Doven in de VEZ</a></li><li><a href="wat-we-doen/groepen/werkverzetters">Werkverzetters</a></li><li><a href="wat-we-doen/groepen/senioren-vez">Senioren VEZ</a></li><li><a href="wat-we-doen/groepen/dubbelgoed">Dubbelgoed </a></li><li><a href="wat-we-doen/groepen/mingles">MINGLES</a></li>
                            </ul>
                        </li>
                    </ul>
                </li><li class="divider show-for-small"></li>
                <li class=" has-dropdown">
                    <a href="betrokken-raken">Betrokken raken</a>
                    <ul class="dropdown">
                        <li class="first">
                            <a>Deelnemen</a>
                            <ul>
                                <li><a href="betrokken-raken/registreren/registratieformulier">Registreren</a></li><li><a href="betrokken-raken/registreren/Thuis in de VEZ">THUIS in de VEZ</a></li><li><a href="betrokken-raken/registreren/huiskring">Huiskring</a></li><li><a href="betrokken-raken/registreren/privacyverklaring">Privacyverklaring</a></li>
                            </ul>
                        </li><li class="second">
                            <a>Bidden & Geven</a>
                            <ul>
                                <li><a href="betrokken-raken/bidden-geven/bidden">Bidden</a></li><li><a href="betrokken-raken/bidden-geven/gebed-in-de-vez">Gebed in de VEZ</a></li><li><a href="betrokken-raken/bidden-geven/geven">Geven</a></li>
                            </ul>
                        </li><li class="third">
                            <a>VEZ Vrijwilligers </a>
                            <ul>
                                <li><a href="betrokken-raken/vez-vrijwilligers/dienen-toppunt">TOPpunt (VEZ vrijwilligersbureau)</a></li><li><a href="betrokken-raken/vez-vrijwilligers/vacatures-toppunt">TOPpunt vacatures</a></li><li><a href="betrokken-raken/vez-vrijwilligers/open-sollicitatie-toppunt">TOPpunt Open solliciteren</a></li><li><a href="betrokken-raken/vez-vrijwilligers/toppunt-spreekuur-">TOPpunt Spreekuur </a></li><li><a href="betrokken-raken/vez-vrijwilligers/toppunt-handen-pool">TOPpunt Handen Pool</a></li><li><a href="betrokken-raken/vez-vrijwilligers/schoonmaken">Schoonmaken VEZ Centrum</a></li>
                            </ul>
                        </li><li class="fourth">
                            <a>MijnKerk</a>
                            <ul>
                                <li><a href="betrokken-raken/mijnkerk/de-interactieve-kerk">Mijn Kerk (Interactieve Kerk)</a></li><li><a href="betrokken-raken/mijnkerk/mijnkerk-veelgestelde-vragen">MijnKerk: Veelgestelde vragen</a></li>
                            </ul>
                        </li>
                    </ul>
                </li><li class="divider show-for-small"></li>
                <li class="active ">
                    <a href="media">Media</a>

                </li><li class="divider show-for-small"></li>
                <li class=" ">
                    <a href="agenda-kerk-in-zwolle">Agenda</a>

                </li><li class="divider show-for-small"></li>
                <li class=" ">
                    <a href="contact">Contact</a>

                </li>
                <li class="divider show-for-small"></li>
                <li class="hide-for-small">
                    <div class="menuSearch"></div>
                    <div class="searchBox">
                        <div class="searchForm">
                            <form method="post" action="search">
                                <input type="text" name="search_field" id="search_field" placeholder="Type uw trefwoord" />
                                <input type="submit" value="Zoek" />
                            </form>
                            <div class="clear"></div>
                        </div>
                    </div>
                </li>
            </ul>

























        </div>
    </div>
</div>
<!-- End Header and Nav -->



<div id="theme" style="background: url('/golive/thumb.php?src=/userfiles/Image/themes/preek-121156.jpg&zc=5&x1=0&y1=449&x2=1799&y2=848&w=1799&h=399&a=x&q=100') no-repeat top center; background-size: cover !important;">

</div>


<script>
    $(document).ready(function() {
        if($('#selected_sermon').val() != ''){
            var aTag = $("a[name='anchor_"+ $('#selected_sermon').val() +"']");
            if(aTag.length > 0)
                $('html,body').animate({scrollTop: aTag.offset().top-100},'slow');
        }
    });
</script>
<input type="hidden" id="selected_sermon" value="">
<section class="row page">
    <div class="twelve columns">
        <section class="content">
            <nav class="breadcrumbs"><span>Media</span></nav>
            <span class="title gradient-white-to-grey">Media</span>
            <div class="text teachings" id="teachingsContainer">
                <h2>Luister de audio-livestream</h2>

                <? if (!$ice->isStreaming() and !$ice->isServiceTime()): ?>
                    <p>
                        Iedere zondag wordt de dienst van 9.30 uur uitgezonden door middel van een audio livestream op deze pagina. Kom daarvoor terug op zondag tussen 9:30 en 10:50 uur.
                        <? if (date('N') == 7 ): ?>
                            <br />De livestream wordt automatisch gestart zodra deze beschikbaar is.

                        <?endif?>
                    </p>
                <?endif?>

                <? if ($ice->isStreaming()): ?>
                    <p>Op dit moment wordt de dienst uitgezonden en deze kan door middel van een audio livestream worden beluisterd vanaf deze pagina.</p>
                    <audio autoplay controls src="<?=$ice->getStreamUrl()?>">
                        Je browser ondersteunt de audio livestream niet. Gebruik een andere browser.
                    </audio>
                <?endif?>

                <? if (!$ice->isStreaming() and $ice->isServiceTime()): ?>
                    <p style="color: #ff050c; font-weight: bold; margin-top:15px">
                        Er gaat op dit moment iets mis met de livestream. Sorry! We doen ons best zo snel mogelijk weer online te zijn. Als dit niet op tijd meer lukt, kun je later vandaag in ieder geval de preek terugluisteren op deze website.
                    </p>
                <?endif?>



            </div>

        </section>
    </div>
</section>



<!-- Footer -->
<footer class="footer">
    <div class="row">
        <div class="one columns">
            <img src="/img/logo-footer.png" alt="VEZ" />
        </div>
        <div class="three columns">
            <div class="footerColumn">
                <h2>Contact</h2>

                <p><a href="contact/vez-centrum">VEZ Centrum</a><br />
                    Rieteweg 12<br />
                    8041 AK Zwolle<br />
                    <br />
                    [E] secretariaat@vezwolle.nl<br />
                    [T] 038 - 422 94 03<br />
                    <a href="contact/vez-centrum">RESERVEREN VEZ CENTRUM<br />
                        <br />
                        De VEZ is een Evangelische kerk in Zwolle.&nbsp;</a></p>
            </div>

        </div>
        <div class="two columns links">
            <div class="footerColumn">
                <h2>Navigeer</h2>
                <a href="home">Home</a> <a href="actueel">Actueel</a> <a href="kerk-in-zwolle/missie-visie/missie-visie-waarden">Wie we zijn</a> <a href="wat-we-doen/onderwijs">Onderwijs</a><a href="wat-we-doen"> </a><a href="betrokken-raken/bidden-geven/geven">Geven</a> <a href="http://www.vezwolle.nl/prekenenmedia/downloads">Downloads</a><a href="http://www.vezwolle.nl/wat-we-doen/kringen/huiskringmateriaal-seizoen-20162017">Kringmateriaal</a><a href="http://www.vezwolle.nl/wat-we-doen/kringen/huiskringmateriaal-seizoen-20162017"> </a><a href="contact">Contact</a></div>

        </div>
        <div class="two columns links">
            <div class="footerColumn">
                <h2>Snel naar</h2>
                <a href="kerk-in-zwolle/missie-visie/missie-visie-waarden">Onze missie</a> <a href="http://www.vezwolle.nl/prekenenmedia">Online preken beluisteren</a> <a href="agenda-kerk-in-zwolle">Kalender</a> <a href="betrokken-raken/vez-vrijwilligers/vacatures-toppunt">Vacatures</a> <a href="http://www.vezwolle.nl/wat-we-doen/kinderen/speciale-activiteiten">Kinderwerk</a></div>

        </div>
        <div class="three columns social">
            <h2>Volg ons:</h2>
            <a target="_blank" href="https://www.facebook.com/VEZwolle" class="fc-webicon facebook"></a>

            <a target="_blank" href="https://twitter.com/VEZwolle" class="fc-webicon twitter"></a>

            <a target="_blank" href="http://vimeo.com/search?q=VEZwolle" class="fc-webicon vimeo"></a>

            <a target="_blank" href="http://www.youtube.com/channel/UCGobTmf89yI73bkYxBImBlA" class="fc-webicon youtube"></a>
        </div>
        <div class="one columns">
            <a href="#" id="scroll-to-top" class="scroll-top icon-up-open"></a>
        </div>
    </div>
    <div class="row">
        <div class="eleven columns offset-by-one copyright">
            &copy; Vrije Evangelisatie Zwolle | Website by <a href="http://www.gopublic.nl" target="_blank">Gopublic</a>
        </div>
    </div>
</footer>

<!-- Fancybox -->
<link rel="stylesheet" href="/golive/js/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" property=""/>
<script type="text/javascript" src="/golive/js/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>



<!-- Initialize JS Plugins -->
<script src="/js/jquery.carouFredSel-6.1.0-packed.js"></script>
<script src="/js/jquery.touchSwipe.min.js"></script>

<script src="/js/jquery.fitvids.min.js"></script>
<script src="/js/jquery.royalslider.min.js"></script>
<script src="/js/app.js?v=1.0.12.4"></script>

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-41378900-1', 'vezwolle.nl');
    ga('send', 'pageview',{
        'anonymizeIp': true
    });

</script>
<link href="/golive/modules/goform2/css/base.css" type="text/css" rel="stylesheet" />
<link href="/golive/bootstrap/datepicker/css/datepicker.css" rel="stylesheet"><script type="text/javascript" src="/golive/bootstrap/datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/golive/modules/goform2/js/form.js?v=1.54.1.3"></script>

<link rel="stylesheet" href="/golive/js/chosen/chosen.min.css">
<script src="/golive/js/chosen/chosen.jquery.min.js" type="text/javascript"></script>


<script src="golive/js/jQueryFileUploader5.41.0/vendor/jquery.ui.widget.js"></script>
<script src="golive/js/jQueryFileUploader5.41.0/jquery.iframe-transport.js"></script>
<script src="golive/js/jQueryFileUploader5.41.0/jquery.fileupload.js"></script></body>

</html>
