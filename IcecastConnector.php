<?php

class IcecastConnector
{
    /**
     * API Url
     * @var string
     */
    private $url = 'https://stream.gentle-innovations.nl:8443/status-json.xsl';

    /**
     * Current stream url - if available
     * @var string
     */
    private $streamUrl = '';

    /**
     * Currently streaming?
     * @var bool|null
     */
    private $isStreaming = null;

    /**
     * Is stream currently active
     * @return bool
     */
    public function isStreaming()
    {
        if (is_bool($this->isStreaming)) {
            return $this->isStreaming;
        }

        $ctx = stream_context_create(array(
            'http' =>
                array(
                    'timeout' => 5,
                )
        ));

        try {
            $content = json_decode(file_get_contents($this->url, false, $ctx));
        } catch (Exception $e) {
            return false;
        }

        if ($content->icestats->source) {
            $this->setStreamUrl(
                str_replace('http://stream.gentle-innovations.nl:8000', 'https://stream.gentle-innovations.nl:8443', $content->icestats->source->listenurl)
            );
            return true;
        } else {
            return false;
        }

    }

    public function isServiceTime() {
        return date('N') == 7 and date('Hi') > 930 and date('Hi') < 1045;
    }

    /**
     * Set stream url
     * @param string $streamUrl
     */
    public function setStreamUrl($streamUrl)
    {
        $this->streamUrl = $streamUrl;
    }

    /**
     * Get stream url
     * @return string|null
     */
    public function getStreamUrl()
    {
        if ($this->isStreaming()) {
            return $this->streamUrl;
        }
    }
}